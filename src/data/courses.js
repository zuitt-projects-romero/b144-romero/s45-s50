const courseData = [
	{
		id : "wdc001",
		name: "PHP - Laravel",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed quis blandit lacus. Morbi gravida risus eu commodo vestibulum. Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
		price: 45000,
		onOffer : true

	},
	{
		id : "wdc002",
		name: "Python - Django",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed quis blandit lacus. Morbi gravida risus eu commodo vestibulum. Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
		price: 50000,
		onOffer : true

	},
	{
		id : "wdc003",
		name: "Java - Springboot",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed quis blandit lacus. Morbi gravida risus eu commodo vestibulum. Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
		price: 55000,
		onOffer : true

	}

];

export default courseData;