// importing state hook from react
// import { useState, useEffect } from 'react';
import PropTypes from 'prop-types'
import { Button, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom'

export default function CourseCard({ courseProp }) {
    // Checks to see if the data was successfully passed
    // console.log(courseProp);
    // console.log(typeof courseProp);
    const { _id, name, description, price } = courseProp;

    // Use stae hook in this component to be able to store its state
    // States are used to keep track of information related to individual components
    /* Syntax:
            const [getter, setter] = useState(initialGetterValue);

            getter - from the initial getter value
            setter - one that will change the state (to increment or not)
    */

    // const [count, setCount] = useState(0);
    // const [seats, setSeats] = useState(30);
    // const [isOpen, setIsOpen] = useState(false);


    // console.log(useState(0));

    // Function to keep track of the enrolles for a course
    // function enroll() {
    //         setCount(count + 1);
    //         console.log('Enrollees: ' + count);
    //         setSeats(seats - 1);
    //         console.log('Seats: ' + seats);
    // }
  // Define a useEffect Hook to have the CourseCard component perform a certain task after every DOM update
    // useEffect(() => {
    //     if (seats === 0) {
    //         setIsOpen(true);
    //     }
    // }, [seats])


    return (
        <Card>
            <Card.Body className="mb-2">
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {price}</Card.Text>
                <Link className='btn btn-primary' to={`/courses/${_id}`}>Details</Link>
            </Card.Body>
        </Card>
    )
}


